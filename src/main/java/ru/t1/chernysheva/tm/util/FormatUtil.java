package ru.t1.chernysheva.tm.util;

import ru.t1.chernysheva.tm.constant.MeasureConst;

import java.text.DecimalFormat;

public final class FormatUtil {

    private static final String NAME_BYTES_SHORT = "B";

    private static final String NAME_BYTES_LONG = "bytes";

    private static final String NAME_KILOBYTE = "KB";

    private static final String NAME_MEGABYTE = "MB";

    private static final String NAME_GIGABYTE = "MB";

    private static final String NAME_TERABYTE = "MB";

    private static final String SEPARATOR = " ";

    private static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("#.###");

    private static String render(final double bytes) {
        return DECIMAL_FORMAT.format(bytes);
    }

    private static String render(final long bytes, final double size) {
        return render(bytes / size);
    }

    private static String render(final long bytes, final double size, final String name) {
        return render(bytes, size) + SEPARATOR + name;
    }

    private static String render(final long bytes, final String name) {
        return render(bytes) + SEPARATOR + name;
    }

    public static String formatBytes(final long bytes) {
        if ((bytes >= 0) && (bytes < MeasureConst.KB))
            return render(bytes, NAME_BYTES_SHORT);
        if ((bytes >= MeasureConst.KB) && (bytes < MeasureConst.MB)) return render(bytes, MeasureConst.KB, NAME_KILOBYTE);
        if ((bytes >= MeasureConst.MB) && (bytes < MeasureConst.GB)) return render(bytes, MeasureConst.MB, NAME_MEGABYTE);
        if ((bytes >= MeasureConst.GB) && (bytes < MeasureConst.TB)) return render(bytes, MeasureConst.GB, NAME_GIGABYTE);;
        if (bytes >= MeasureConst.TB) return render(bytes, MeasureConst.GB, NAME_TERABYTE);
        return render(bytes, NAME_BYTES_LONG);
    }

    private FormatUtil() {
    }

}
