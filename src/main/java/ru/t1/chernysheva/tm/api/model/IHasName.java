package ru.t1.chernysheva.tm.api.model;

public interface IHasName {

    String getName();

    void setName(String name);

}
