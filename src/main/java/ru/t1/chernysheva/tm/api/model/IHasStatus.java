package ru.t1.chernysheva.tm.api.model;

import ru.t1.chernysheva.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);
}
